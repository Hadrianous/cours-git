# 1 étape #

Petit cours de git pour les basiques

### 1ère étape ###

* Faire un checkout de master (git checkout master)
* Créer une branche nommée feature/add-param (git checkout -b feature/add-param)
* Ajouter un param à la fonction "n" pour que la fonction fasse un "return n" et commit (git add main.py + git commit -m "message de commit 1")

### 2ème étape ###

* Faire un checkout de master (git checkout master)
* Créer une branche nommée feature/multiple-param (git checkout -b feature/multiple-param)
* Faire en sorte que la fonction fasse return var * 2 et commit (git add main.py + git commit -m "message de commit 2")
* Ajouter un autre commit pour faire return var * 3 (git add main.py + git commit -m "message de commit 3")
* Fusionner les 2 commits en faisant un git rebase -i master
* Dans l'interface changer "pick" du 2ème message par "s" ou squash et accepter les changements
* Commenter le message du commit 1 en mettant un # devant et accepter
* Faire un merge de la branche dans master (git checkout master + git merge feature/multiple-param)

### 3ème étape ###

* Faire un checkout de master
* Faire un git pull
* Faire un checkout de notre première branche (git checkout feature/add-param)
* Faire un rebase sur master pour avoir la modif de la var * 3
* Régler le conflit pour que la fonction fasse un return n * 3
* Une fois le conflit gérér, continuer le rebase (git rebase --continue)
* Faire le merge dans master